<?php


namespace App\Service\Elasticsearch;


use Elastica\Query\Match;

abstract class BaseElasticSearchService
{
    private const FRENCH_SEARCH_ANALYSER = 'custom_french_analyzer';

    protected function setMatchFieldWithFrenchAnalyzer($searchText, $field, $boost = 1.0) {
        $fieldQuery = new Match();
        $fieldQuery->setFieldQuery($field, $searchText);
        $fieldQuery->setFieldParam($field, 'analyzer', self::FRENCH_SEARCH_ANALYSER );
        $fieldQuery->setFieldBoost($field, $boost);
        return $fieldQuery;
    }
}
