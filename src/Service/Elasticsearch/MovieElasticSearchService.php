<?php


namespace App\Service\Elasticsearch;


use Elastica\Query\BoolQuery;
use Elastica\Query\Match;
use FOS\ElasticaBundle\Finder\PaginatedFinderInterface;

class MovieElasticSearchService extends BaseElasticSearchService
{
    /**
     * @var PaginatedFinderInterface
     */
    private $finder;

    public function __construct(PaginatedFinderInterface $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @return array
     */
    public function searchMovie($searchTitle, $searchId, $searchDirector, $searchActor, $searchReleaseDate): array
    {
        $boolQuery = new BoolQuery();

        $boolQuery->addShould($this->setMatchFieldWithFrenchAnalyzer($searchTitle, 'title', 10));
        $boolQuery->addShould($this->setMatchFieldWithFrenchAnalyzer($searchId, 'id', 10));
        $boolQuery->addShould($this->setMatchFieldWithFrenchAnalyzer($searchDirector, 'director', 5));
        $boolQuery->addShould($this->setMatchFieldWithFrenchAnalyzer($searchActor, 'actors', 5));

        return $this->finder->find($boolQuery);
    }
}
