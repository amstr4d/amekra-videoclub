<?php


namespace App\Service;


use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class TheMovieDBApiService
{
    /**
     * @var HttpClient
     */
    private $httpClient;
    /**
     * @var ContainerBagInterface
     */
    private $params;

    public function __construct(ContainerBagInterface $params, HttpClientInterface $httpClient)
    {

        $this->httpClient = $httpClient;
        $this->params = $params;
    }


    /**
     * @param $query
     * @return array
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function movieSearch($query): array
    {
        $movieSearch = $this->httpClient->request('GET', 'https://api.themoviedb.org/3/search/movie', [
            'query' => [
                'api_key' => $this->params->get('tmdb_api_key'),
                'language' => $this->params->get('locale'),
                'query' => $query
            ]
        ]);

        $this->waitRatelimitingReset($movieSearch);

        return $movieSearch->toArray();
    }

    /**
     * @param $apiMovieId
     * @return array
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function getMovieDetails($apiMovieId): array
    {
        $movieDetails = $this->httpClient->request('GET', 'https://api.themoviedb.org/3/movie/'. $apiMovieId, [
            'query' => [
                'api_key' => $this->params->get('tmdb_api_key'),
                'language' => $this->params->get('locale'),
            ]
        ]);

        $this->waitRatelimitingReset($movieDetails);

        return $movieDetails->toArray();
    }

    /**
     * @param $apiMovieId
     * @return array
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function getMovieCrew($apiMovieId): array
    {
        $movieCredits = $this->httpClient->request('GET', 'https://api.themoviedb.org/3/movie/'. $apiMovieId .'/credits', [
            'query' => [
                'api_key' => $this->params->get('tmdb_api_key'),
                'language' => $this->params->get('locale'),
            ]
        ]);

        $this->waitRatelimitingReset($movieCredits);

        return $movieCredits->toArray();
    }

    /**
     * @param string $apiPosterPath
     * @param string $customFileName
     * @return UploadedFile
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function retrieveMoviePosterFile(string $apiPosterPath, string $customFileName = ''): UploadedFile
    {
        $tempPostersPath =  $this->params->get('kernel.project_dir') . '/public' . $this->params->get('app.path.movie_temp_posters');

        if(!is_dir($tempPostersPath)){
            mkdir($tempPostersPath, 0755, true);
        }
        if($customFileName !== '') {
            $postFilePath = $tempPostersPath . $customFileName;
        } else {
            $postFilePath = $tempPostersPath . $apiPosterPath;
        }

        if(!file_exists($postFilePath)) {
            $req = $this->httpClient->request('GET', 'https://image.tmdb.org/t/p/w500' . $apiPosterPath);
            file_put_contents($postFilePath, $req->getContent());
        }

        return new UploadedFile($postFilePath, $apiPosterPath, 'image/jpeg', null, true);
    }

    /**
     * @param ResponseInterface $response
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    private function waitRatelimitingReset(ResponseInterface $response): void
    {
        $responseHeaders = $response->getHeaders();
        if(array_key_exists('x-ratelimit-remaining', $responseHeaders)) {
            $rateLimitRemaining = (int) $responseHeaders['x-ratelimit-remaining'][0];
            if($rateLimitRemaining === 1) {
                sleep(10);
            }
        }
    }

}
