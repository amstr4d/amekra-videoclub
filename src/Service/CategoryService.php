<?php


namespace App\Service;


use App\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;

class CategoryService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * CategoryService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getAll(): array
    {
        return $this->entityManager->getRepository(Category::class)->findAll();
    }

    /**
     * @param $id
     * @return Category|object|null
     * @throws EntityNotFoundException
     */
    public function getById($id)
    {
        $movie = $this->entityManager->getRepository(Category::class)->find($id);
        if(!$movie) {
            throw new EntityNotFoundException('Movie with id ' . $id . ' not found');
        }
        return $movie;
    }
}
