<?php


namespace App\Service;


use App\Entity\Category;
use App\Entity\Movie;
use App\Repository\MovieRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Psr\Log\LoggerInterface;

class MovieService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var MovieRepository
     */
    private $movieRepository;
    /**
     * @var TheMovieDBApiService
     */
    private $theMovieDBApiService;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * MovieService constructor.
     * @param EntityManagerInterface $entityManager
     * @param MovieRepository $movieRepository
     * @param TheMovieDBApiService $theMovieDBApiService
     */
    public function __construct(EntityManagerInterface $entityManager, MovieRepository $movieRepository, TheMovieDBApiService $theMovieDBApiService, LoggerInterface $logger)
    {
        $this->entityManager = $entityManager;
        $this->movieRepository = $movieRepository;
        $this->theMovieDBApiService = $theMovieDBApiService;
        $this->logger = $logger;
    }

    /**
     * @param null $offset
     * @param null $limit
     * @return array
     */
    public function getAll($offset = null, $limit = null): array
    {
        return $this->entityManager->getRepository(Movie::class)->findBy([], ['id' => 'DESC'], $limit, $offset);
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getAllQueryBuilder(): \Doctrine\ORM\QueryBuilder
    {
        return $this->movieRepository->getAllQueryBuilder();
    }

    /**
     * @param $id
     * @return Movie|object|null
     * @throws EntityNotFoundException
     */
    public function getById($id)
    {
        $movie = $this->entityManager->getRepository(Movie::class)->find($id);
        if(!$movie) {
            throw new EntityNotFoundException('Movie with id ' . $id . ' not found');
        }
        return $movie;
    }

    /**
     * @param $categoryId
     * @param null $offset
     * @param null $limit
     * @return mixed
     * @throws EntityNotFoundException
     */
    public function getByCategoryId($categoryId, $offset = null, $limit = null)
    {
        $category = $this->entityManager->getRepository(Category::class)->find($categoryId);
        if(!$category) {
            throw new EntityNotFoundException('Category with id ' . $categoryId . ' not found');
        }

        return $this->entityManager->getRepository(Movie::class)->findMoviesByCategory($category->getId(), $offset, $limit);
    }

    /**
     * @param $categoryId
     * @return \Doctrine\ORM\QueryBuilder
     * @throws EntityNotFoundException
     */
    public function getByCategoryIdQb($categoryId)
    {
        $category = $this->entityManager->getRepository(Category::class)->find($categoryId);
        if(!$category) {
            throw new EntityNotFoundException('Category with id ' . $categoryId . ' not found');
        }

        return $this->entityManager->getRepository(Movie::class)->findMoviesByCategoryQb($category->getId());
    }

    /**
     * @param Movie[] $movies
     * @return int
     */
    public function importAllMovieDetails(array $movies): int
    {
        $movieCount = 0;
        foreach ($movies as $movie) {
            $this->importMovieDetails($movie);
            $movieCount++;
            if($movieCount === 10) {
                try {
                    $this->entityManager->flush();
                } catch (\Exception $e) {
                    $this->logger->error('Error importing movie ' . $movie->getTitle());
                    if (!$this->entityManager->isOpen()) {
                        $this->entityManager = $this->entityManager->create(
                            $this->entityManager->getConnection(),
                            $this->entityManager->getConfiguration()
                        );
                    }
                }
                $movieCount = 0;
            }
        }

        try {
            $this->entityManager->flush();
        } catch (\Exception $e) {
            $this->logger->error('Error importing movie ' . $movie->getTitle());
            if (!$this->entityManager->isOpen()) {
                $this->entityManager = $this->entityManager->create(
                    $this->entityManager->getConnection(),
                    $this->entityManager->getConfiguration()
                );
            }
        }

        return count($movies);
    }

    /**
     * @param Movie $movie
     */
    public function importMovieDetails(Movie $movie): void
    {
        $movieTitle = $movie->getTitle();
        $content = $this->theMovieDBApiService->movieSearch($movieTitle);

        if(empty($content['results'])) {
            $this->logger->warning('No results for movie with title : "' . $movieTitle . '"');
            return;
        }
        $apiMovieId = $content['results'][0]['id'];

        $this->updateMovieDetails($movie, $apiMovieId);

        $this->entityManager->persist($movie);
    }

    /**
     * @param Movie $movie
     * @param $apiMovieId
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function updateMovieDetails(Movie $movie, $apiMovieId): void
    {
        $movieDetailsArray = $this->theMovieDBApiService->getMovieDetails($apiMovieId);

        $movie->setSummary($movieDetailsArray['overview']);
        $movie->setDuration($movieDetailsArray['runtime']);
        $date = DateTime::createFromFormat('Y-m-d', $movieDetailsArray['release_date']);
        if($date) {
            $movie->setReleaseDate($date);
        }
        $movie->setReview($movieDetailsArray['vote_average']);

        if(!empty($movieDetailsArray['poster_path'])) {
            $posterFile = $this->theMovieDBApiService->retrieveMoviePosterFile($movieDetailsArray['poster_path']);
            $movie->setPosterFile($posterFile);
        } else {
            $this->logger->info('No poster found for movie with title : "' . $movie->getTitle() . '"');
        }

        // Credits

        $movieCreditsArray = $this->theMovieDBApiService->getMovieCrew($apiMovieId);

        $actors = [];
        $filteredCastArray = array_slice($movieCreditsArray['cast'], 0, 5);
        foreach ($filteredCastArray as $cast) {
            $actors[] = $cast['name'];
        }
        $movie->setActors($actors);
        $key = array_search('Director', array_column($movieCreditsArray['crew'], 'job'), true);
        if((is_string($key) || is_int($key)) && array_key_exists($key, $movieCreditsArray['crew'])) {
            $director = $movieCreditsArray['crew'][$key]['name'];
            $movie->setDirector($director);
        }

        $movie->setIsUpdated(true);
    }
}
