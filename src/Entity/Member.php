<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MemberRepository")
 */
class Member
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;


    /**
     * @ORM\Column(type="datetime")
     */
    private $registrationDate;

    /**
     * @ORM\Column(type="integer")
     */
    private $balance = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $card = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $free = false;

    /**
     * @ORM\Column(type="integer")
     */
    private $countDelayCanceled = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $fineDelay = 0;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Rental", mappedBy="r_member", cascade={"persist"}, orphanRemoval=true)
     */
    private $rentals;

    public function __construct()
    {
        $this->rentals = new ArrayCollection();
        $this->registrationDate = new DateTime();
    }

    public function __toString()
    {
        return $this->id . ' - ' . $this->firstname . ' ' . $this->lastname;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getRegistrationDate(): ?\DateTimeInterface
    {
        return $this->registrationDate;
    }

    public function setRegistrationDate(\DateTimeInterface $registrationDate): self
    {
        $this->registrationDate = $registrationDate;

        return $this;
    }

    public function getBalance(): ?int
    {
        return $this->balance;
    }

    public function setBalance(int $balance): self
    {
        $this->balance = $balance;

        return $this;
    }

    public function getCard(): ?bool
    {
        return $this->card;
    }

    public function setCard(bool $card): self
    {
        $this->card = $card;

        return $this;
    }

    public function getFree(): ?bool
    {
        return $this->free;
    }

    public function setFree(bool $free): self
    {
        $this->free = $free;

        return $this;
    }

    public function getCountDelayCanceled(): ?int
    {
        return $this->countDelayCanceled;
    }

    public function setCountDelayCanceled(int $countDelayCanceled): self
    {
        $this->countDelayCanceled = $countDelayCanceled;

        return $this;
    }

    public function getFineDelay(): ?int
    {
        return $this->fineDelay;
    }

    public function setFineDelay(int $fineDelay): self
    {
        $this->fineDelay = $fineDelay;

        return $this;
    }

    /**
     * @return Collection|Rental[]
     */
    public function getRentals(): Collection
    {
        return $this->rentals;
    }

    public function addRental(Rental $rental): self
    {
        if (!$this->rentals->contains($rental)) {
            $this->rentals[] = $rental;
            $rental->setRMember($this);
        }

        return $this;
    }

    public function removeRental(Rental $rental): self
    {
        if ($this->rentals->contains($rental)) {
            $this->rentals->removeElement($rental);
            // set the owning side to null (unless already changed)
            if ($rental->getRMember() === $this) {
                $rental->setRMember(null);
            }
        }

        return $this;
    }
}
