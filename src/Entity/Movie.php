<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MovieRepository")
 * @Vich\Uploadable
 */
class Movie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"public"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"public"})
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Rental", mappedBy="movie")
     * @Groups({"public"})
     */
    private $rentals;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="movies")
     * @Groups({"public"})
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"public"})
     */
    private $director;


    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"public"})
     */
    private $releaseDate;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"public"})
     */
    private $summary;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"public"})
     */
    private $review;

    /**
     * @ORM\Column(type="boolean")
     */
    private $acquired;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateAcquired;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"public"})
     */
    private $updateAt;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $invoiced = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"public"})
     */
    private $poster;

    /**
     * @Vich\UploadableField(mapping="movie_posters", fileNameProperty="poster")
     * @var File
     */
    private $posterFile;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    private $isUpdated = false;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"public"})
     */
    private $duration;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Groups({"public"})
     */
    private $actors = [];

    public function __construct()
    {
        $this->updateAt = new \DateTime();
        $this->rentals = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->id . ' - ' . $this->title;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection|Rental[]
     */
    public function getRentals(): Collection
    {
        return $this->rentals;
    }

    public function addRental(Rental $rental): self
    {
        if (!$this->rentals->contains($rental)) {
            $this->rentals[] = $rental;
            $rental->setMovie($this);
        }

        return $this;
    }

    public function removeRental(Rental $rental): self
    {
        if ($this->rentals->contains($rental)) {
            $this->rentals->removeElement($rental);
            // set the owning side to null (unless already changed)
            if ($rental->getMovie() === $this) {
                $rental->setMovie(null);
            }
        }

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getDirector(): ?string
    {
        return $this->director;
    }

    public function setDirector(?string $director): self
    {
        $this->director = $director;

        return $this;
    }


    public function getReleaseDate(): ?\DateTimeInterface
    {
        return $this->releaseDate;
    }

    public function setReleaseDate(?\DateTimeInterface $releaseDate): self
    {
        $this->releaseDate = $releaseDate;

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(?string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

    public function getReview(): ?float
    {
        return $this->review;
    }

    public function setReview(?float $review): self
    {
        $this->review = $review;

        return $this;
    }

    public function getAcquired(): ?bool
    {
        return $this->acquired;
    }

    public function setAcquired(bool $acquired): self
    {
        $this->acquired = $acquired;

        return $this;
    }

    public function getDateAcquired(): ?\DateTimeInterface
    {
        return $this->dateAcquired;
    }

    public function setDateAcquired(?\DateTimeInterface $dateAcquired): self
    {
        $this->dateAcquired = $dateAcquired;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getIsMovieOut()
    {
        $lastRental = $this->getRentals()->last();
        if($lastRental) {
            return $lastRental->getDateBack() === null;
        }

        return false;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getInvoiced(): ?bool
    {
        return $this->invoiced;
    }

    public function setInvoiced(bool $invoiced): self
    {
        $this->invoiced = $invoiced;

        return $this;
    }

    public function getPoster(): ?string
    {
        return $this->poster;
    }

    public function setPoster(?string $poster): self
    {
        $this->poster = $poster;

        return $this;
    }

    public function setPosterFile(?File $image = null)
    {
        $this->posterFile = $image;

        if ($image) {
            $this->updateAt = new \DateTime('now');
        }

        return $this;
    }

    public function getPosterFile()
    {
        return $this->posterFile;
    }

    public function getIsUpdated(): ?bool
    {
        return $this->isUpdated;
    }

    public function setIsUpdated(bool $isUpdated): self
    {
        $this->isUpdated = $isUpdated;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(?int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getActors(): ?array
    {
        return $this->actors;
    }

    public function setActors(?array $actors): self
    {
        $this->actors = $actors;

        return $this;
    }
}
