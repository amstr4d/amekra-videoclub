<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RentalRepository")
 */
class Rental
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"public"})
     */
    private $dateOut;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"public"})
     */
    private $dateBack;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Movie", inversedBy="rentals")
     * @ORM\JoinColumn(nullable=false)
     */
    private $movie;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Member", inversedBy="rentals")
     * @ORM\JoinColumn(nullable=false)
     */
    private $r_member;

    public function __construct()
    {
        $this->dateOut = new DateTime();
    }

    public function __toString()
    {
        return '' . $this->getMovie()->getId() . ' - ' . $this->getMovie()->getTitle();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateOut(): ?\DateTimeInterface
    {
        return $this->dateOut;
    }

    public function setDateOut(\DateTimeInterface $dateOut): self
    {
        $this->dateOut = $dateOut;

        return $this;
    }

    public function getDateBack(): ?\DateTimeInterface
    {
        return $this->dateBack;
    }

    public function setDateBack(?\DateTimeInterface $dateBack): self
    {
        $this->dateBack = $dateBack;

        return $this;
    }

    public function getMovie(): ?Movie
    {
        return $this->movie;
    }

    public function setMovie(?Movie $movie): self
    {
        $this->movie = $movie;

        return $this;
    }

    public function getRMember(): ?Member
    {
        return $this->r_member;
    }

    public function setRMember(?Member $r_member): self
    {
        $this->r_member = $r_member;

        return $this;
    }
}
