<?php


namespace App\Form\Type;


use App\Entity\Member;
use App\Form\MovieEmbeddedForm;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\EasyAdminFormType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class RentalAdminFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('member', EntityType::class, [
                'class' => Member::class,
                'attr' => ['data-widget' => 'select2'],
            ])
            ->add('movies', CollectionType::class, [
                'entry_type' => MovieEmbeddedForm::class,
                'entry_options' => ['label' => false],
                'allow_delete' => true,
                'allow_add' => true,
                'by_reference' => false,

            ])
            ->add('dateOut', DateType::class, [
                'format' => 'dd/MM/yyyy H:mm',
                'widget' => 'single_text',
                'data' => new \DateTime()
            ])
            ->add('submit', SubmitType::class);
    }
}
