<?php


namespace App\Form;


use App\Entity\Movie;
use App\Repository\MovieRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class MovieEmbeddedForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('movie', EntityType::class, [
                'class' => Movie::class,
                'choice_label' => function ($movie) {
                    return $movie->getId() . ' - ' . $movie->getTitle();
                },
                'attr' => ['data-widget' => 'select2'],
                'query_builder' => function (MovieRepository $er) {
                    return $er->getAllMoviesInStock();
                },
            ]);
    }

}
