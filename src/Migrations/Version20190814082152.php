<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190814082152 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');


        $this->addSql('ALTER TABLE rental DROP FOREIGN KEY FK_1619C27D7597D3FE');
        $this->addSql('DROP INDEX IDX_1619C27D7597D3FE ON rental');
        $this->addSql('ALTER TABLE rental CHANGE member_id r_member_id INT NOT NULL');
        $this->addSql('ALTER TABLE rental ADD CONSTRAINT FK_1619C27D1648404B FOREIGN KEY (r_member_id) REFERENCES member (id)');
        $this->addSql('CREATE INDEX IDX_1619C27D1648404B ON rental (r_member_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE rental DROP FOREIGN KEY FK_1619C27D1648404B');
        $this->addSql('DROP INDEX IDX_1619C27D1648404B ON rental');
        $this->addSql('ALTER TABLE rental CHANGE r_member_id member_id INT NOT NULL');
        $this->addSql('ALTER TABLE rental ADD CONSTRAINT FK_1619C27D7597D3FE FOREIGN KEY (member_id) REFERENCES member (id)');
        $this->addSql('CREATE INDEX IDX_1619C27D7597D3FE ON rental (member_id)');
    }
}
