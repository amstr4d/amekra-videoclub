<?php

namespace App\Repository;

use App\Entity\Movie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Movie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Movie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Movie[]    findAll()
 * @method Movie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MovieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Movie::class);
    }

    public function getAllQueryBuilder()
    {
        return $this->createQueryBuilder('m')
            ->select('m')
            ->orderBy('m.id', 'DESC');
    }

    /**
     * @return QueryBuilder
     */
    public function getAllMoviesInStock()
    {
        $qb = $this->createQueryBuilder('m');
        $qb->leftJoin('m.rentals', 'r')
            ->add('where', $qb->expr()->in('r.id',
                $this->createQueryBuilder('m2')
                    ->select('MAX(r2.id)')
                    ->from('App:Rental', 'r2')
                    ->where('r2.movie = m')
                    ->getDQL()
            ))
            ->andWhere('r.dateBack IS NOT NULL')
            ->orWhere('r.id IS NULL');

        return $qb;
    }

    public function findMoviesByCategoryQb($categoryId)
    {
        $qb = $this->createQueryBuilder('m')
            ->innerJoin('m.category', 'c')
            ->addSelect('c')
            ->andWhere('c.id = :id')
            ->setParameter('id', $categoryId)
            ->addOrderBy('m.id', 'DESC');
        return $qb;
    }

    public function findMoviesByCategory($categoryId, $offset, $limit)
    {
        $qb = $this->findMoviesByCategoryQb($categoryId);

        if($offset) {
            $qb->setFirstResult($offset);
        }

        if($limit) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }
}
