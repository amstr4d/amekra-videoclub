<?php


namespace App\Controller\Api;


use App\Entity\Movie;
use App\Service\Elasticsearch\MovieElasticSearchService;
use App\Service\MovieService;
use Doctrine\ORM\EntityNotFoundException;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Knp\Component\Pager\PaginatorInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Swagger\Annotations as SWG;

class MovieController extends AbstractFOSRestController
{
    /**
     * @var MovieService
     */
    private $movieService;
    /**
     * @var PaginatorInterface
     */
    private $paginator;
    /**
     * @var MovieElasticSearchService
     */
    private $searchService;

    /**
     * MovieController constructor.
     * @param MovieService $movieService
     * @param PaginatorInterface $paginator
     * @param MovieElasticSearchService $searchService
     */
    public function __construct(MovieService $movieService, PaginatorInterface $paginator, MovieElasticSearchService $searchService)
    {
        $this->movieService = $movieService;
        $this->paginator = $paginator;
        $this->searchService = $searchService;
    }

    /**
     * @Rest\Get("/movies", name="getAllMovies")
     * @Rest\View(serializerGroups={"public"})
     * @Rest\QueryParam(name="offset", requirements="\d+", default=null, nullable=true)
     * @Rest\QueryParam(name="limit", requirements="\d+", default=null, nullable=true)
     * @SWG\Response(
     *     response="200",
     *     description="Return all movies",
     *     @Model(type=Movie::class)
     *     )
     * @param ParamFetcherInterface $paramFetcher
     * @return View
     */
    public function getAll(ParamFetcherInterface $paramFetcher): View
    {
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');

        $movieEntities = $this->movieService->getAll($offset, $limit);
        return View::create($movieEntities, Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/movies/list", name="listAllMovies")
     * @Rest\View(serializerGroups={"public"})
     * @Rest\QueryParam(name="page", requirements="\d+", default=1)
     * @param ParamFetcherInterface $paramFetcher
     * @return View
     */
    public function listAll(ParamFetcherInterface $paramFetcher): View
    {
        $page = $paramFetcher->get('page');

        $movieQb = $this->movieService->getAllQueryBuilder();
        $pagination = $this->paginator->paginate(
            $movieQb,
            $page,
        );

        return View::create($pagination, Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/movies/search", name="searchMovie")
     * @Rest\QueryParam(name="page", requirements="\d+", default=1)
     * @Rest\QueryParam(name="title")
     * @Rest\QueryParam(name="id")
     * @Rest\QueryParam(name="director")
     * @Rest\QueryParam(name="actor")
     * @Rest\QueryParam(name="releaseDate")
     * @Rest\View(serializerGroups={"public"})
     * @param ParamFetcherInterface $paramFetcher
     * @return View
     */
    public function searchMovie(ParamFetcherInterface $paramFetcher)
    {
        $page = $paramFetcher->get('page');
        $title = $paramFetcher->get('title');
        $id = $paramFetcher->get('id');
        $director = $paramFetcher->get('director');
        $actor = $paramFetcher->get('actor');
        $releaseDate = $paramFetcher->get('releaseDate');


        $results = $this->searchService->searchMovie($title, $id, $director, $actor, $releaseDate);

        $pagination = $this->paginator->paginate(
            $results,
            $page,
            );

        return View::create($pagination, Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/movies/{id}", name="getMovieById")
     * @Rest\View(serializerGroups={"public"})
     * @param int $id
     * @return View
     * @throws EntityNotFoundException
     */
    public function getById(int $id): View
    {
        $movieEntity = $this->movieService->getById($id);
        return View::create($movieEntity, Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/movies/category/{categoryId}", name="getAllMoviesByCategory")
     * @Rest\View(serializerGroups={"public"})
     * @Rest\QueryParam(name="offset", requirements="\d+", default="")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="")
     * @SWG\Response(
     *     response="200",
     *     description="Return all movies in a category",
     *     @Model(type=Movie::class)
     *     )
     * @param int $categoryId
     * @return View
     * @throws EntityNotFoundException
     */
    public function getByCategoryId(ParamFetcherInterface $paramFetcher, int $categoryId): View
    {
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');
        $movieEntities = $this->movieService->getByCategoryId($categoryId, $offset, $limit);
        return View::create($movieEntities, Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/movies/list/category/{categoryId}", name="listAllMoviesByCategory")
     * @Rest\QueryParam(name="page", requirements="\d+", default=1)
     * @Rest\View(serializerGroups={"public"})
     * @param ParamFetcherInterface $paramFetcher
     * @param int $categoryId
     * @return View
     * @throws EntityNotFoundException
     */
    public function listByCategoryId(ParamFetcherInterface $paramFetcher, int $categoryId): View
    {
        $page = $paramFetcher->get('page');
        $movieQb = $this->movieService->getByCategoryIdQb($categoryId);

        $pagination = $this->paginator->paginate(
            $movieQb,
            $page,
            );

        return View::create($pagination, Response::HTTP_OK);
    }
}
