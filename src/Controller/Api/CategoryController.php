<?php


namespace App\Controller\Api;


use App\Service\CategoryService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Swagger\Annotations as SWG;

class CategoryController extends AbstractFOSRestController
{
    /**
     * @var CategoryService
     */
    private $categoryService;

    /**
     * CategoryController constructor.
     * @param CategoryService $categoryService
     */
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * @Rest\Get("/categories", name="getAllCategories")
     * @Rest\View(serializerGroups={"public"})
     * @SWG\Response(
     *     response="200",
     *     description="Return all categories",
     *     @Model(type=Category::class)
     *     )
     * @return View
     */
    public function getAll(): View
    {
        $categories = $this->categoryService->getAll();
        return View::create($categories, Response::HTTP_OK);
    }
}
