<?php

namespace App\Controller;

use App\Entity\Member;
use App\Entity\Movie;
use App\Entity\Rental;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends EasyAdminController
{
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    protected function persistEntity($entity)
    {
        if (method_exists($entity, 'setUpdateAt')) {
            $entity->setUpdateAt(new \DateTime());
        }

        parent::persistEntity($entity);
    }

    public function updateEntity($entity)
    {
        if (method_exists($entity, 'setUpdateAt')) {
            $entity->setUpdateAt(new \DateTime());
        }

        parent::updateEntity($entity);
    }

    /**
     * @Route("/dashboard", name="admin_dashboard")
     */
    public function dashboard()
    {
        $movieCount = $this->em->getRepository(Movie::class)->count([]);
        $memberCount = $this->em->getRepository(Member::class)->count([]);
        $rentalCount = $this->em->getRepository(Rental::class)->count([]);
        return $this->render('admin/dashboard.html.twig', [
            'movie_count' => $movieCount,
            'member_count' => $memberCount,
            'rental_count' => $rentalCount,
        ]);
    }
}
