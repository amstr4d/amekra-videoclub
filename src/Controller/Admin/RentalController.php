<?php


namespace App\Controller\Admin;


use App\Entity\Rental;
use App\Form\Type\RentalAdminFormType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;

class RentalController extends EasyAdminController
{
    protected function newAction()
    {
        $entity = $this->executeDynamicMethod('createNew<EntityName>Entity');
        $easyadmin = $this->request->attributes->get('easyadmin');
        $easyadmin['item'] = $entity;
        $this->request->attributes->set('easyadmin', $easyadmin);

        $fields = $this->entity['new']['fields'];


        $form = $this->createForm(RentalAdminFormType::class);

        $form->handleRequest($this->request);
        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            foreach($data['movies'] as $movie) {
                $rental = new Rental();
                $rental->setRMember($data['member']);
                $rental->setMovie($movie['movie']);
                $rental->setDateOut($data['dateOut']);
                $this->em->persist($rental);
            }

            $this->em->flush();
            return $this->redirectToReferrer();
        }

        $parameters = [
            'form' => $form->createView(),
            'entity_fields' => $fields,
            'entity' => $entity,
        ];

        return $this->render('admin/rental.html.twig', $parameters);
    }

    public function movieBackAction()
    {
        $id = $this->request->query->get('id');
        $entity = $this->em->getRepository(Rental::class)->find($id);
        $entity->setDateBack(new \DateTime());
        $this->em->flush();

        // redirect to the 'list' view of the given entity ...
        return $this->redirectToRoute('easyadmin', array(
            'action' => 'list',
            'entity' => $this->request->query->get('entity'),
        ));
    }

}
