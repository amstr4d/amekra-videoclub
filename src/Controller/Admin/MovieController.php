<?php


namespace App\Controller\Admin;


use App\Entity\Movie;
use App\Service\MovieService;
use App\Service\TheMovieDBApiService;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class MovieController extends EasyAdminController
{
    /**
     * @var TheMovieDBApiService
     */
    private $theMovieDBApiService;
    /**
     * @var MovieService
     */
    private $movieService;
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * MovieController constructor.
     * @param MovieService $movieService
     * @param TheMovieDBApiService $theMovieDBApiService
     * @param TranslatorInterface $translator
     * @param LoggerInterface $logger
     */
    public function __construct(MovieService $movieService, TheMovieDBApiService $theMovieDBApiService, TranslatorInterface $translator, LoggerInterface $logger)
    {
        $this->theMovieDBApiService = $theMovieDBApiService;
        $this->movieService = $movieService;
        $this->translator = $translator;
        $this->logger = $logger;
    }

    public function importAction()
    {
        $searchResults = [];
        $id = $this->request->query->get('id');
        $movie = $this->em->getRepository(Movie::class)->find($id);
        if($movie === null) {
            return $this->redirectToReferrer();
        }

        $form = $this->createFormBuilder()
            ->add('selectedMovie', HiddenType::class)
            ->getForm();

        $form->handleRequest($this->request);

        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            try {
                $this->movieService->updateMovieDetails($movie, $data['selectedMovie']);
            } catch(TransportExceptionInterface $e) {
                $this->addFlash('danger', $this->translator->trans('admin.import.transport_error'));
                $this->logger->error('Transport error', [$e]);
                return $this->redirectToReferrer();
            } catch (\Throwable $e) {
                $this->addFlash('danger', $this->translator->trans('admin.import.common_error'));
                $this->logger->error('Common error', [$e]);
                return $this->redirectToReferrer();
            }

            $this->em->persist($movie);
            $this->em->flush();
            $this->addFlash('success', $this->translator->trans('admin.import.success'));
            return $this->redirectToReferrer();
        }

        $movieTitle = $movie->getTitle();
        try {
            $searchResults = array_slice($this->theMovieDBApiService->movieSearch($movieTitle)['results'], 0, 5);
        } catch(TransportExceptionInterface $e) {
            $this->addFlash('danger', $this->translator->trans('admin.import.transport_error'));
            $this->logger->error('Transport error', [$e]);
            return $this->redirectToReferrer();
        } catch (\Throwable $e) {
            $this->addFlash('danger', $this->translator->trans('admin.import.common_error'));
            $this->logger->error('Common error', [$e]);
            return $this->redirectToReferrer();
        }

        return $this->render('admin/importMovieAdmin.html.twig', [
            'searchResults' => $searchResults,
            'form' => $form->createView()
        ]);
    }
}
