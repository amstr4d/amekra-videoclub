<?php


namespace App\Controller\Admin;


use App\Entity\Member;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Contracts\Translation\TranslatorInterface;

class MemberController extends EasyAdminController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function confirmResetAllAction()
    {
        return $this->render('admin/confirmResetAllMembers.html.twig');
    }

    public function resetAllAction()
    {
        $members = $this->em->getRepository(Member::class)->findAll();

        foreach ($members as $member) {
            $member->setBalance(0);
            $this->em->persist($member);
        }

        $this->em->flush();
        $this->addFlash('success', $this->translator->trans('admin.member.reset.success'));
        return $this->redirectToRoute('easyadmin', ['entity' => 'Members', 'action' => 'list']);
    }
}
