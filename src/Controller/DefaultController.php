<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/")
     * @Route("/{route}", requirements={"route"="^(?!.*api|admin|_wdt|_profiler).+"})
     */
    public function indexAction(): Response
    {
        return $this->render('index.html.twig');
    }
}
