<?php

namespace App\Tests\Service;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Service\TheMovieDBApiService;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class TheMovieDBApiServiceTest extends WebTestCase
{
    private $theMovieDBApiService;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->theMovieDBApiService = self::$container->get(TheMovieDBApiService::class);
    }

    public function testMovieSearch()
    {
        $query = $this->theMovieDBApiService->movieSearch('Avengers');
        $this->assertIsArray($query);
        $this->assertArrayHasKey('results', $query);
        $this->assertArrayHasKey('id', $query['results'][0]);

    }

    public function testMovieDetails()
    {
        $query = $this->theMovieDBApiService->getMovieDetails(24428);
        $this->assertIsArray($query);
        $this->assertArrayHasKey('overview', $query);
        $this->assertArrayHasKey('runtime', $query);
        $this->assertArrayHasKey('release_date', $query);
        $this->assertArrayHasKey('vote_average', $query);
    }

    public function testMovieCrew()
    {
        $query = $this->theMovieDBApiService->getMovieCrew(24428);
        $this->assertIsArray($query);
        $this->assertArrayHasKey('cast', $query);
        $this->assertArrayHasKey('crew', $query);
    }

    public function testRetrievePosterFile()
    {
        $projectDir = self::$container->getParameter('kernel.project_dir');
        $query = $this->theMovieDBApiService->retrieveMoviePosterFile('/s9UPgyelWtEqjS3HT3TUuHU9BHU.jpg', '/test.jpg');
        $this->assertEquals('test.jpg', $query->getFilename());
        $this->assertEquals($projectDir . '/public/uploads/images/temp_posters', $query->getPath());
        $this->assertFileExists($projectDir . '/public/uploads/images/temp_posters/test.jpg');
        unlink($projectDir . '/public/uploads/images/temp_posters/test.jpg');
    }

}
