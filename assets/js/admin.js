import '../css/admin.scss';

const selectInputs = document.querySelectorAll('input[name="selectMovie"]');
const hiddenInput = document.querySelector('#form_selectedMovie');

selectInputs.forEach(element => {
  element.addEventListener('click', event => {
    hiddenInput.value = element.value;
  });
});

