import Vue from 'vue'
import 'bootstrap'
import '../css/main.scss'

import dayjs from 'dayjs';
import 'dayjs/locale/fr';
import relativeTime from 'dayjs/plugin/relativeTime';
import LocalizedFormat from 'dayjs/plugin/localizedFormat';
import utc from 'dayjs/plugin/utc';

dayjs.locale('fr');
dayjs.extend(relativeTime);
dayjs.extend(LocalizedFormat);
dayjs.extend(utc);
Vue.prototype.$dayjs = dayjs;

import App from '../vue/App'
import router from '../vue/router/router'

const app = new Vue({
  el: '#app',
  router,
  render: h => h(App)
});
