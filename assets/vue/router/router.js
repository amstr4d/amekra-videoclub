import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import(/* webpackChunkName: "movieList" */ '../components/Movie/MovieList.vue'),
      props: (route) => ({ currentPageParam: route.query.page })
    },
    {
      path: '/category/:categoryId',
      name: 'categoryList',
      component: () => import(/* webpackChunkName: "movieList" */ '../components/Movie/MovieList.vue'),
      props: true
    },
    {
      path: '/movie/:id',
      name: 'movie',
      component: () => import(/* webpackChunkName: "movieDetails" */ '../components/Movie/MovieDetails.vue'),
      props: true
    },
    {
      path: '/search',
      name: 'search',
      component: () => import(/* webpackChunkName: "searchPage" */ '../components/Search/SearchPage.vue'),
    },
    {
      path: '*',
      redirect: { name: 'home' }
    }
  ]
});

export default router
