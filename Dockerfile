FROM node:12-slim AS ui-deps
WORKDIR /opt/build
ADD . .
RUN npm ci && npm run build

FROM kibatic/symfony:7.3
ENV SYMFONY_VERSION 4
RUN apt-get update \
    && apt-get install -y php7.3-zip php7.3-mysql php7.3-opcache php7.3-apcu php7.3-xdebug \
    && apt-get clean

ADD ./docker/php/conf/php.ini /usr/local/etc/php/conf.d/20-docker.ini
ADD . /var/www
WORKDIR /var/www

RUN composer install -o --no-suggest
COPY --from=ui-deps /opt/build/public/build ./public/build
