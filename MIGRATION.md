SELECT T_Adherents.NumAd, T_Adherents.Nom, T_Adherents.Prenom, T_Adherents.Telephone, T_Adherents.Adresse_mail, T_Inscriptions.Encaisse, T_Inscriptions.CartePoint, T_Inscriptions.Gratuit, T_Inscriptions.NbrRetardAnnule, T_Inscriptions.AmendeRetard, MAX(T_Inscriptions.Date_inscription) as 'Date_inscription' FROM `T_Adherents`
JOIN T_Inscriptions
ON T_Adherents.NumAd = T_Inscriptions.NumAd
GROUP BY T_Inscriptions.NumAd

